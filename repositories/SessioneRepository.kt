package cc.carmineza.CMS.repositories

import cc.carmineza.CMS.models.Sessione
import org.springframework.data.repository.CrudRepository

interface SessioneRepository : CrudRepository<Sessione, Long> {
    fun findByToken(token: String): Sessione

}