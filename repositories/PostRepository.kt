package cc.carmineza.CMS.repositories

import cc.carmineza.CMS.models.Post
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository

interface PostRepository : CrudRepository<Post, Long> {
}