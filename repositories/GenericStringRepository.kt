package cc.carmineza.CMS.repositories

import cc.carmineza.CMS.models.GenericString
import org.springframework.data.repository.CrudRepository

interface GenericStringRepository : CrudRepository<GenericString, Long> {
    fun findByName(name: String): List<GenericString>
}
