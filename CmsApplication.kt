package cc.carmineza.CMS

import cc.carmineza.CMS.models.GenericString
import cc.carmineza.CMS.models.Post
import cc.carmineza.CMS.models.Sessione
import cc.carmineza.CMS.models.Utente
import cc.carmineza.CMS.repositories.GenericStringRepository
import cc.carmineza.CMS.repositories.PostRepository
import cc.carmineza.CMS.repositories.SessioneRepository
import cc.carmineza.CMS.repositories.UtenteRepository
import groovy.lang.GroovySystem
import org.apache.commons.logging.Log
import org.springframework.boot.Banner
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.context.annotation.Configuration
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView
import org.springframework.stereotype.Controller
import java.time.LocalDateTime
import org.springframework.web.servlet.view.groovy.GroovyMarkupViewResolver
import org.springframework.web.servlet.view.groovy.GroovyMarkupConfigurer
import org.springframework.web.servlet.config.annotation.EnableWebMvc


@SpringBootApplication
class CmsApplication

class PostRicevuto (
        var token: String,
        var lead: String,
        var titolo: String,
        var idautore: Long,
        var testo: String
)

class StringaAutorizzata(
        var token: String,
        var stringa: String
)

class UsernameAndPassword(
        var username: String,
        var password: String
)

@RestController
class UtenteController (val repository: UtenteRepository, val repoSessione: SessioneRepository) {

    @PostMapping("/api/login")
    fun accedi(@RequestBody post: UsernameAndPassword): String {
        val nickName = post.username
        val segreto = post.password
        val utenti = repository.findByNickNameAndPassword(nickName, segreto)
        if(! utenti.isEmpty()){
            var token = ""
            val caratteri = "-.,+#<>={}|^@$&*/!0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
            for(i in 1..64) {
                token+=caratteri[Math.floor(Math.random() * caratteri.length).toInt()]
            }
            val sessione = Sessione(token, utenti[0].id)
            repoSessione.save(sessione)
            return token
        }
        return "false"
    }

    @GetMapping("/api/utente/{id}")
    fun trovaUtente(@PathVariable id: Long) =
            repository.findById(id)
    @GetMapping("/api/utente/nome/{nome}")
    fun trovaUtente(@PathVariable nome: String) =
            repository.findByNickName(nome)
}

class GenericStringController(val repository: GenericStringRepository, val repoSessione: SessioneRepository) {
    @PostMapping("/api/cambianome")
    fun cambiaNome(@RequestBody nome: StringaAutorizzata) : Unit{
        if(repoSessione.findByToken(nome.token).userid > 0) {
            repository.deleteById(repository.findByName("nomeBlog")[0].id)
            repository.save(
                    GenericString(
                            "nomeBlog",
                            nome.stringa
                    )
            )
        }
    }
    @GetMapping("/api/impostanome/{nome}")
    fun impostaNome(@PathVariable nome: String): Unit {
        if(repository.findByName("nomeBlog").isNotEmpty()) repository.save(GenericString("nomeBlog", nome))
    }
    @GetMapping("/api/nomeblog")
    fun nome(): String {
        return repository.findByName("nomeBlog")[0].value
    }
}

@RestController
class PostController (val repository: PostRepository, val repoSessione: SessioneRepository, val repoUtenti: UtenteRepository, val string: GenericStringRepository) {

    @PostMapping("/api/pubblica")
    fun inviaMessaggio(@RequestBody post: PostRicevuto): Any {
        if(repoSessione.findByToken(post.token).userid == post.idautore) {
            return repository.save(Post(
                    System.currentTimeMillis() / 1000,
                    post.lead,
                    post.titolo,
                    post.testo,
                    post.idautore
            ))
        }
        return "false"
    }

    @GetMapping("/api/posts")
    fun findAllPosts() =
            repository.findAll()

    @GetMapping("/api/id/{id}")
    fun findPostById(@PathVariable id: Long) =
            repository.findById(id)

    @GetMapping("/posts/{id}")
    fun renderPostById(@PathVariable id: Long, model: ModelAndView): ModelAndView {
        val post = findPostById(id).get()
        System.out.println(post.titolo)
        System.out.println(post.idautore)
        val nomeAutore = repoUtenti.findById(post.idautore).get().nickName
        System.out.println(nomeAutore)
        model.viewName = "post"
        model.model.put("nomeBlog", string.findByName("nomeBlog")[0].value)
        model.model.put("nomePost", post.titolo)
        model.model.put("lead", post.lead)
        model.model.put("autore", nomeAutore)
        model.model.put("testo", post.testo)
        return model
    }
    @GetMapping("/")
    fun homePage(model: ModelAndView): ModelAndView {
        model.viewName = "index"
        model.model.put("nomeBlog", string.findByName("nomeBlog")[0].value)
        model.model.put("posts", findAllPosts())
        return model
    }
}

@SpringBootApplication
class Application {
    /*
        @Bean
        fun passwordEncoder(): PasswordEncoder {
            return BCryptPasswordEncoder()
        }
    */
    @Bean
    fun init(posts: PostRepository, users: UtenteRepository, string: GenericStringRepository) = CommandLineRunner {
        posts.save(Post(
                System.currentTimeMillis() / 1000,
                "Prova Post CMS",
                "primo articolo",
                "Questo è un articolo di prova dal DB",
                1
        ))
        posts.save(Post(
                System.currentTimeMillis() / 1000,
                "Seconda Prova Post CMS",
                "secondo articolo",
                "Questo è un altro un articolo di prova dal DB",
                1
        ))
        posts.save(Post(
                System.currentTimeMillis() / 1000,
                "Un'altra prova",
                "terzo articolo",
                "Un altro articolo, prova di come si può modificare tutto e tutte le pagine in modo semplice",
                1
        ))
        users.save(Utente(
                "Carmine",
                "Zaccagnino",
                "carminezacc",
                "pizza",
                1
        ))
        string.save(GenericString(
                "nomeBlog",
                "Prova 1 2 3 4 5 6 7 8"
        ))
    }
}


fun main(args: Array<String>) {
    runApplication<CmsApplication>(*args)
}
